import 'package:flutter/material.dart';
import 'package:cartoons_flutter/model/character.dart';

class CharacterWidget extends StatelessWidget {
  final Character character;

  const CharacterWidget({Key? key, required this.character}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    //TODO 0: Usa widgets básicos que ya conoces para crear este widget/cell.
    // Algunos consejos útiles:
    // * No elimine el widget Container(), agregue todos sus widgets dentro. Recuerde que el widget Container
    //   tiene un solo hijo, pero su primer paso probablemente debería ser agregar un widget Row como ese hijo.
    // * Cargue imágenes con Image.asset (character.image, ...
    // * En el widget Container, siéntase libre de usar decoration:
    //   BoxDecoration (color: Colors.black12, borderRadius: BorderRadius.all (Radius.circular (20.0)))

    return Container(
        height: 150,
        margin: const EdgeInsets.symmetric(vertical: 7, horizontal: 10),
        child: Card(
          color: Colors.black12,
          shape: const RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(20.0))),
          child: Padding(
            padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
            child: Row(
              children: [
                Image.asset(
                  character.image,
                ),
                Container(
                  padding: const EdgeInsets.only(bottom: 10.0, top: 10.0),
                  child: Column(
                    children: [
                      Text(character.name,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          )),
                      Container(
                        child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              CircleAvatar(
                                backgroundColor: character.stars < 4
                                    ? Colors.blue
                                    : Colors.green,
                                child: Text(character.stars.toString(),
                                    style: const TextStyle(
                                        fontSize: 14.0,
                                        fontWeight: FontWeight.normal,
                                        color: Colors.white)),
                              ),
                              const SizedBox(width: 10),
                              Text(character.jobTitle,
                                  style: const TextStyle(
                                      fontSize: 15.0, color: Colors.black)),
                            ]),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}
